from django.urls import path
from django.urls import re_path
from . import views

urlpatterns = [
    path('', views.home, name='blog-home'),
    path('about/', views.about, name='blog-about'),
    re_path(r'^.*$', views.test_redirect, name='test_redirect'),
]