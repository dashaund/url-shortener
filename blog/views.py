from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound, Http404,  HttpResponseRedirect
import url_shortener

posts = [
    {
        'author': 'CoreyMS',
        'title': 'Blog Post 1',
        'content': 'First post content',
        'date_posted': 'August 27, 2018',
    },
    {
        'author': 'Jane Doe',
        'title': 'Blog Post 2',
        'content': 'Second post content',
        'date_posted': 'August 28, 2019',
    }
]

title = {
    'title': "about"
}

def home(request):
    data = request.POST.get('name')
    print(data)
    if (data != None):
        url = url_shortener.genURL(data)
    else:
        url = ""
    return render(request, 'blog/blog.html', {'data':url})


def about(request):
    return render(request, 'blog/about.html', title)


def test_redirect(request):
    path = request.get_full_path()
    stourl = url_shortener.retURL(path)
    return HttpResponseRedirect(stourl)